
var gulp = require('gulp'),
    ngAnnotate = require('gulp-ng-annotate'),
    concat = require('gulp-concat'),
    templateCache = require('gulp-angular-templatecache'),
    watch = require('gulp-watch'),
    sourcemaps = require('gulp-sourcemaps'),
    minifyCss = require('gulp-minify-css');

var paths = {
    scripts: 'src/js/*',
    styles: 'src/styles/*',
    templates: 'templates/**/*.html'
};

gulp.task('scripts', function () {
    return gulp.src([paths.scripts])
        .pipe(sourcemaps.init())
            .pipe(ngAnnotate())
            .pipe(concat('app.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('static/frontend'));
});

gulp.task('styles', function () {
    return gulp.src([paths.styles])
        .pipe(minifyCss())
        .pipe(concat('styles.css'))
        .pipe(gulp.dest('static/frontend'));
});

gulp.task('templates', function () {
    return gulp.src('templates/**/*.html')
        .pipe(templateCache({
            module: 'app'
        }))
        .pipe(gulp.dest('static/frontend'));
});

gulp.task('vendors_js', function () {
    return gulp.src([
            'vendors/jquery/dist/jquery.min.js',
            'vendors/angular/angular.min.js',
            'vendors/angular-route/angular-route.min.js',
            'vendors/angular-resource/angular-resource.min.js',
            'vendors/angular-ui-router/release/angular-ui-router.min.js'
        ])
        .pipe(sourcemaps.init({loadMaps: true}))
            .pipe(concat('vendors.js'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('static/frontend'));
});

gulp.task('vendors_style', function() {
    return gulp.src([
        'vendors/bootstrap/dist/css/bootstrap.min.css'
    ])
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(concat('theme.css'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('static/frontend'));
});



gulp.task('watch', function() {
    gulp.watch(paths.scripts, ['scripts']);
    gulp.watch(paths.styles, ['styles']);
    gulp.watch(paths.templates, ['templates']);
});

gulp.task('default', ['watch', 'scripts', 'styles', 'templates']);
gulp.task('build', ['scripts', 'templates', 'vendors_js', 'vendors_style']);