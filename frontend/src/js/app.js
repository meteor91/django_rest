angular.module('app', ['ngResource', 'ui.router']);

angular.module('app')
    .config(function($stateProvider, $urlRouterProvider) {
        $urlRouterProvider.otherwise("/")
        $stateProvider
            .state('home', {
                url: '/',
                templateUrl: 'pages/home.html'
            })
            .state('add', {
                url: '/add',
                templateUrl: 'pages/create_test.html'
            })
            .state('sign_in', {
                url: '/sign-in',
                templateUrl: 'pages/sign.html'
            })
            .state('show_tests', {
                url: '/show-tests',
                templateUrl: 'pages/tests.html'
            })
    })
    .factory('Answers', ['$resource', function($resource) {
        return $resource('/tests/v1/answers/:id.json');
    }])
    .factory('Test', ['$resource', function($resource) {
        return $resource('/tests/v1/tests/:id.json');
    }])
    .service('Auth', function ($http, $location, $q, $window) {
        var Auth = {
                getToken: function () {
                    return $window.localStorage.getItem('token');
            },

            setToken: function (token) {
                $window.localStorage.setItem('token', token);
            },

            deleteToken: function () {
                $window.localStorage.removeItem('token');
            },

            login: function (username, password) {
                var deferred = $q.defer();

                $http.post('/sign/api/v1/auth/login/', {
                    username: username, password: password
                }).success(function (response, status, headers, config) {
                    if (response.token) {
                        Auth.setToken(response.token);
                    }

                    deferred.resolve(response, status, headers, config);
                }).error(function (response, status, headers, config) {
                    deferred.reject(response, status, headers, config);
                });

                return deferred.promise;
            },

            logout: function () {
                Auth.deleteToken();
                $window.location = '/';
            },
            register: function (user) {
                var deferred = $q.defer();

                $http.post('/sign/api/v1/auth/register/', {
                    user: user
                }).success(function (response, status, headers, config) {
                    Auth.login(user.username, user.password).
                    then(function (response, status, headers, config) {
                        $window.location = '/';
                    });

                    deferred.resolve(response, status, headers, config);
                }).error(function (response, status, headers, config) {
                    deferred.reject(response, status, headers, config);
                });
              return deferred.promise;
            }
        };

        return Auth;
    })
    .service('AuthInterceptor', function ($injector, $location) {
        var AuthInterceptor = {
            request: function (config) {
                var Auth = $injector.get('Auth');
                var token = Auth.getToken();

                if (token) {
                    config.headers['Authorization'] = 'JWT ' + token;
                }
                return config;
            },

            responseError: function (response) {
                if (response.status === 403) {
                    $location.path('/login');
                }
                return response;
            }
        };
        return AuthInterceptor;
    })
    .config(function($httpProvider) {
        $httpProvider.interceptors.push('AuthInterceptor');
    })
    .directive('startCreateTestForm', function() {
        function link(scope, element, attrs) {
            scope.onSubmit = function() {
                scope.$emit('test.create.start', {
                    id: scope.id,
                    name: scope.name
                })
            }
        }

        return {
            link: link,
            templateUrl: 'directives/start_create_test_form.html',
            scope: {
                type: '@'
            }
        }
    })
    .directive('addQuestion', function() {
        function link(scope, element, attrs) {
            scope.onSubmit = function() {
                scope.$emit('test.create.add.question', {
                    id: scope.id,
                    text: scope.text
                })
            }
        }

        return {
            link: link,
            templateUrl: 'directives/add_question.html',
            scope: {
                id: '@'
            }
        }
    })
    .directive('testCreateStateView', function() {
        return {
            templateUrl: 'directives/test_create_state_view.html',
            scope: {
                test: '='
            }
        }
    });