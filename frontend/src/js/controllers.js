angular.module('app')
    .controller('ctrl', ['$scope', 'Answers', function($scope, Answers) {
        $scope.name='polly';
        $scope.val = function() {return 'some val';}
        $scope.id = 1;
        $scope.answer = null;
        $scope.getAnswer = function() {
            var a = Answers.get({id: $scope.id}, function() {
                console.log(a);
                $scope.answer = a;
            })
        };
        Answers.query(function(responce) {
            $scope.answers = responce;
        });
    }])
    .controller('singInFormController', function($scope, Auth) {
        $scope.submit=function() {
            console.log($scope.username);
            console.log($scope.password);
            Auth.login($scope.username, $scope.password);
        }
    })
    .controller('testsCtrl', function($scope, Test) {
        Test.query(function(responce) {
            $scope.tests = responce;
        });

    })
    .controller('createTestController', function($scope, $compile, Test) {
        $scope.createEmptyTest = function(name) {
            console.log(name);
        };
        var newScope = $scope.$new();
        angular.element('.forms-container').append($compile('<start-create-test-form type="update"></start-create-test-form>')(newScope));

        $scope.test = new Test();
        var on_create = $scope.$on('test.create.start', function(event, data) {
            $scope.test.name=data.name;
            $scope.test.$save(function(response) {
                angular.element('.forms-container').empty();
                newScope.$destroy();
                console.log('id = '+$scope.test.id);
                angular.element('.test-container').append(
                    $compile('<test-create-state-view test="test"></test-create-state-view>')($scope.$new())
                );
                var id = $scope.test.id;
                angular.element('.forms-container').append(
                    $compile('<add-question id="'+id+'"></add-question>')($scope.$new())
                );
            });
        });

        var on_question_add = $scope.$on('test.create.add.question', function(event, data) {

        })


    });