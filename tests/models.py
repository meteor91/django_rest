from django.db import models


class Question(models.Model):
    text = models.CharField(max_length=255)
    created = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ('created',)


class Answer(models.Model):
    created = models.DateTimeField(auto_now_add=True)
    text = models.CharField(max_length=255)
    question = models.ForeignKey(Question)
    correct = models.BooleanField(default=False)

    class Meta:
        ordering = ('created',)


class Test(models.Model):
    name = models.CharField(max_length=255)
    questions = models.ManyToManyField(Question)
