from django.conf.urls import url
from tests import views
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = [
    url(r'^v1/tests/$', views.TestList.as_view()),
    url(r'^v1/tests/(?P<pk>[0-9]+)/$', views.TestDetail.as_view()),
    url(r'^v1/answers/$', views.AnswerList.as_view()),
    url(r'^v1/answers/(?P<pk>[0-9]+)/$', views.AnswerDetail.as_view()),
    url(r'^v1/questions/$', views.QuestionList.as_view()),
]

urlpatterns = format_suffix_patterns(urlpatterns)